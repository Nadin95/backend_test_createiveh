<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductLine extends Model
{
    use HasFactory;

    private $table='productlines';

    public function products()
    {
        $this->hasMany(Product::class,'productLine');
    }
}
