<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table='orders';


    public function Customer()
    {
        return $this->belongsTo(Customer::class,'customerNumber','customerNumber');
    }

    public function OrderDetail()
    {
        return $this->hasMany(OrderDetail::class,'orderNumber','orderNumber');
    }



}
