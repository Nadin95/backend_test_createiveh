<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;
    protected $table='orderdetails';

    public function order()
    {
        return $this->hasMany(Order::class,'orderNumber','orderNumber');
    }

    public function product()
    {
       return $this->belongsTo(Product::class,'productCode','productCode');
    }



}
