<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\OrderRepository;
use App\Models\Order;


class OrdersController extends Controller
{



    /**
     * fetch order details
     * @param $id
     * @return array
     */

    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository=$orderRepository;

    }


    public function fetchOrderData($id)
    {
        // your logic goes here.

        $order=$this->orderRepository->getOrder($id);
        return $order;










    }

}
