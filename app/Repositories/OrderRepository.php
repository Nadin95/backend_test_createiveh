<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Customer;
use Illuminate\Http\Response;
use App\Repositories\OrderRepository;
use DB;

class OrderRepository{


    public function getOrder($id)
    {


        if(Order::where('orderNumber','=',$id)->exists()){


        $order_orderdetail=order::where('orderNumber',$id)->with('OrderDetail')->first();
        $Orderdetail_product=OrderDetail::where('orderNumber',$id)->with('product')->get();
        $total_bill=OrderDetail::where('orderNumber','=',$id)
        ->select(DB::raw('sum(orderdetails.priceEach * orderdetails.quantityOrdered) as bill_amount'))->first();
        $Order_customer=Order::where('orderNumber',$id)->with('Customer')->first();


        $order_list = [];

        foreach($Orderdetail_product as $order){

        $order_list [] = array(
          'product'=>$order->product->productName,
          'product_line'=>$order->product->productLine,
          'unit_price'=>$order->priceEach,
          'qty'=>$order->quantityOrdered,
          'line_total'=>number_format($order->priceEach*$order->quantityOrdered,2),

      );
        }

        $object=array(
            'order_id'=>$order_orderdetail->orderNumber,
            'order_date'=>$order_orderdetail->orderDate,
            'status'=>$order_orderdetail->status,
            'order_details'=>$order_list,
            'bill_amount'=>$total_bill['bill_amount'],
            'customer'=>[
                        'first_name'=>$Order_customer->Customer->contactFirstName,
                        'last_name'=>$Order_customer->Customer->contactLastName,
                        'phone'=>$Order_customer->customer->phone,
                        'country_code'=>$Order_customer->customer->country

                        ]

        );

                return response()->json($object, 200);
        }else{
                return response()->json(['Message' => 'Order Not Found'], 400);
        }






}



}
